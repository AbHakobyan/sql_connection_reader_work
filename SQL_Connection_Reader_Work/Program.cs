﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SQL_Connection_Reader_Work
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create SQL Stream string
            string conStr = @"Data Source = .\SQLEXPRESS; Initial Catalog = ShopDB; Integrated Security = true;";
            
            //Create SQL Connection 
            SqlConnection connection = new SqlConnection(conStr);

            //Open SQL Connection
            connection.Open();

            //Create SQL Command 
            SqlCommand cmd = new SqlCommand("SELECT * FROM Custumers", connection);

            //ExecuteReader
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                //Read All Lines  Read() method
                while (reader.Read())
                {
                    
                    //4
                    //Hakobyan Albert Razmiki
                    //Armavir
                    //(91)342235
                    Console.WriteLine(reader[0]);//index[4]
                    Console.WriteLine(reader[2] + " " + reader[1] + " " + reader[3]);//SFF - Hakobyan Albert Razmiki
                    Console.WriteLine(reader[5]);//Region  Armavir
                    Console.WriteLine(reader[6]);//Phone Number (91)342235
                    Console.WriteLine(new string('-',20));
                }
            }
            //Close SQL Connection
            connection.Close();
        }
    }
}
